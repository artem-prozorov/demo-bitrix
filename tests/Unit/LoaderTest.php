<?php

use PHPUnit\Framework\TestCase;

class LoaderTest extends TestCase
{
    public function testLoader()
    {
        $this->assertTrue(\Bitrix\Main\Loader::includeModule('iblock'));
    }
}