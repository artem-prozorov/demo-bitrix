<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

require_once realpath(__DIR__.'/../../../vendor/autoload.php');
